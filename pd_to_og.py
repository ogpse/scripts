import requests
from collections import defaultdict

OG_KEY = ''
OG_HEADERS = {'Authorization': 'GenieKey ' + OG_KEY}
PD_KEY = ''
PD_HEADERS = {'Accept': 'application/vnd.pagerduty+json;version=2', 'Authorization': 'Token token=' + PD_KEY}
OG_ACCOUNT = ''
PD_ACCOUNT = ''

og_integrations_types_dict = {'Amazon CloudWatch': 'CloudWatch',
                              'events_api_v2_inbound_integration': 'API',
                              'Events API v1': 'API',
                              'Fabric': 'API',
                              'New Relic': 'NewRelic',
                              'Nagios': 'API',
                              'Jenkins CI': 'Jenkins',
                              'generic_email_inbound_integration': 'Email',
                              'Sumo Logic': 'SumoLogic',
                              'generic_events_api_inbound_integration': 'API',
                              'vCenter Server': 'VCenter',
                              'Splunk (legacy)': 'Splunk',
                              'Pingdom': 'PingdomV2',
                              'Pingdom Email': 'Email',
                              'Keynote': 'Email',
                             }

MAX_TO_RETURN = 500

def pagerduty_get_all(api_request, list_key):
    items = []
    page = 0
    while True:
        url = api_request + f'&offset={page}&total=true'
        result = requests.get(url, headers=PD_HEADERS).json()
        items.extend(result[list_key])
        page += result['limit']
        if page >= result['total'] or page >= MAX_TO_RETURN:
            break
    return items

tickets = {}
pd_orphaned = {}
og_orphaned = {}

services = pagerduty_get_all(api_request='https://api.pagerduty.com/services?time_zone=UTC&sort_by=name&include%5B%5D=integrations',
                             list_key='services')

integration_counts = defaultdict(int)
pd_service_ids = defaultdict(list)

for service in services:

    if not service['teams']:
        pd_orphaned[service['summary']] = {}
        pd_orphaned[service['summary']]['pd_url'] = service['html_url']
        continue

    team_name = service['teams'][0]['summary']
    pd_service_ids[team_name] += [service['id']]

    if team_name not in tickets:
        tickets[team_name] = {}
        tickets[team_name]['pd_url'] = service['teams'][0]['html_url']
    if 'integrations' not in tickets[team_name]:
        tickets[team_name]['integrations'] = {}

    for integration in service['integrations']:
        metadata = {}
        url = f'https://api.pagerduty.com/services/{service["id"]}/integrations/{integration["id"]}?include%5B%5D=vendors'
        detail = requests.get(url, headers=PD_HEADERS).json()
        vendor = detail['integration']['vendor']
        if not vendor:
            vendor = {'summary': integration['type']}
        integration_counts[vendor['summary']] += 1
        metadata['pd_type'] = vendor['summary']
        metadata['pd_summary'] = integration['summary']
        metadata['pd_url'] = detail['integration']['html_url']
        metadata['pd_service'] = service['summary']
        if 'integration_key' in detail['integration']:
            metadata['pd_key'] = detail['integration']['integration_key']
        if 'integration_email' in detail['integration']:
            metadata['pd_key'] = detail['integration']['integration_email']

        tickets[team_name]['integrations'][service['summary'] + ' ' + integration['summary']] = metadata


def opsgenie_get_all(api_request):
    items = []
    while True:
        result = requests.get(api_request, headers=OG_HEADERS).json()
        items.extend(result['data'])
        if 'paging' in result['data']:
            print(result)
            exit()
        #no pagination??
        break
    return items

og_teams = opsgenie_get_all('https://api.opsgenie.com/v2/teams')
og_team_names = {}
og_team_ids = {}
for team in og_teams:
    #underscores in OpsGenie for some reason, need to match PD
    og_team_names[team['id']] = team['name']
    og_team_ids[team['name']] = team['id']
og_integrations = opsgenie_get_all("https://api.opsgenie.com/v2/integrations")
og_integrations_dict = {}

for integration in og_integrations:
    #Generic OG integration
    if not integration['teamId']:
        continue

    team_name = og_team_names[integration['teamId']]
    if team_name not in tickets:
        og_orphaned[team_name] = {}
        og_orphaned[team_name]['og_url'] = f'https://' + OG_ACCOUNT + '.app.opsgenie.com/teams/dashboard/{integration["teamId"]}/integrations'
        continue
    og_integrations_dict[integration['name']] = {}
    og_integrations_dict[integration['name']]['id'] = integration['id']
    og_integrations_dict[integration['name']]['type'] = integration['type']
    og_integrations_dict[integration['name']]['teamId'] = integration['teamId']
    r = requests.get(f'https://api.opsgenie.com/v2/integrations/{integration["id"]}', headers=OG_HEADERS).json()
    if 'apiKey' in integration:
        og_integrations_dict[integration['name']]['apiKey'] = integration['apiKey']
    if 'emailAddress' in integration:
        og_integrations_dict[integration['name']]['emailAddress'] = integration['emailAddress']

for team, team_values in tickets.items():
    for integration, integration_values in team_values['integrations'].items():
        integration_type = integration_values['pd_type']
        if integration_values['pd_type'] in og_integrations_types_dict:
            #Translate to OpsGenie terminology if needed
            integration_type = og_integrations_types_dict[integration_values['pd_type']]
        #This should be adjusted 
        integration_name = f'{team} {integration_values["pd_service"]} {integration_values["pd_summary"]} migrated from PagerDuty'
        print(integration_name)
        #if we already have this integration, just note it
        #TODO: how do we get the apiKey or emailAddress back out?
        #https://docs.opsgenie.com/docs/integration-api#section-create-api-based-integration
        if integration_name in og_integrations_dict:
            #tickets[team]['integrations'][integration]['og_key'] = og_integrations_dict[integration_name]['apiKey']
            tickets[team]['integrations'][integration]['og_url'] = f'https://' + OG_ACCOUNT + '.app.opsgenie.com/integration?teamId={og_team_ids[team]}#/edit/{integration_type}/{og_integrations_dict[integration_name]["id"]}'
            continue
        if integration_type == 'Live Call Routing':
            tickets[team]['integrations'][integration]['og_url'] = f'https://' + OG_ACCOUNT + '.app.opsgenie.com/integration?teamId={og_team_ids[team]}#/add/IncomingCall'
            continue
        if integration_type == 'Datadog':
            tickets[team]['integrations'][integration]['og_url'] = f'https://' + OG_ACCOUNT + '.app.opsgenie.com/integration?teamId={og_team_ids[team]}#/add/Datadog'
            continue
        if integration_type == 'Slack to PagerDuty':
            tickets[team]['integrations'][integration]['og_url'] = f'https://' + OG_ACCOUNT + '.app.opsgenie.com/integration?teamId={og_team_ids[team]}#/add/SlackApp'
            continue
        if integration_type == 'ServiceNow (Legacy)':
            tickets[team]['integrations'][integration]['og_url'] = f'https://' + OG_ACCOUNT + '.app.opsgenie.com/integration?teamId={og_team_ids[team]}#/add/ServiceNowV3'
            continue
        if integration_type == 'LogicMonitor':
            tickets[team]['integrations'][integration]['og_url'] = f'https://' + OG_ACCOUNT + '.app.opsgenie.com/integration?teamId={og_team_ids[team]}#/add/LogicMonitor'
            continue
        #otherwise create og integration, record that
        #raise Exception('Hold on before creating integrations...')
        data = {'type': integration_type, 'name': integration_name, 'ownerTeam': {'name': team}}
        if integration_type == 'Email':
            data['emailUsername'] = integration_values['pd_key'].split('@')[0]
        #print(data)
        r = requests.post('https://api.opsgenie.com/v2/integrations', headers=OG_HEADERS, json=data)
        if r.status_code != 201:
            if 'already exists' in r.json()['message']:
                tickets[team]['integrations'][integration]['og_url'] = f'https://' + OG_ACCOUNT + '.app.opsgenie.com/integration?teamId={og_team_ids[team]}#/add/{integration_type}'
                continue
            print(r.json())
            exit()

        result = r.json()

        #if 'apiKey' in r['data']:
        #tickets[team]['integrations'][integration]['og_key'] = 'sample key'#r['data']['apiKey']
        #if 'emailAddress' in r['data']:
        #tickets[team]['integrations'][integration]['og_key'] = 'sample email'#r['data']['emailAddress']
        tickets[team]['integrations'][integration]['og_url'] = f'https://' + OG_ACCOUNT + 'app.opsgenie.com/integration?teamId={og_team_ids[team]}#/edit/{integration_type}/{result["data"]["id"]}'
        #break
    #break

for team, team_values in tickets.items():
    print(f'\n\n\nh1. {team}')
    print(f'h2. 1. Migration')
    print('To migrate each integration below, you must update the source of your alerts with a new OpsGenie Key, to replace the old PagerDuty Key (referenced in the table below).')
    print('For details on how to migrate and validate each one, carefully read the instruction guide found at the New OpsGenie URL. If you are unfamiliar with configuring your alert sources, please work with the engineer who configured your alerts.')
    print('If your team has manually created any of these integrations in OpsGenie already, you may ignore and/or remove those that overlap. The list below are suggested migrations which have been automatically prepared for you. To confirm your final list, see part 2 Validation below.')
    print(f'||Integration||New OpsGenie URL||Old PagerDuty URL||Old PagerDuty Key||')
    for integration, integration_values in team_values['integrations'].items():
        extra = ''
        if '#/add/' in integration_values["og_url"]:
            extra = 'MANUAL CREATION NEEDED, follow link, hit Save: '
        print(f'|{integration}|{extra}[{integration_values["og_url"]}]|[{integration_values["pd_url"]}]|{integration_values["pd_key"]}|')
    print('h2. 2. Validation')
    print('Once you are done, please make sure everything here is safe to discard by April 2:')
    for service in pd_service_ids[team]:
        print(f'* [https://' + PD_ACCOUNT + '.pagerduty.com/services/{service}/integrations]')
    print('\nAnd ensure this list is complete and validated by your team:')
    print(f' * [https://' + OG_ACCOUNT + 'app.opsgenie.com/teams/dashboard/{og_team_ids[team]}/integrations]')
    print('NOTE: You are responsible for validating that your newly configured integrations are working correctly with your alert sources!!')
    print('Technical questions can be fielded in slack in #opsgenie-support')

#print(og_orphaned)
#print(pd_orphaned)


#For each PD team, for each integration
# Create OpsGenie integration
# Print out link to PD integration, PD key
# Print out link to OG integration, OG key
