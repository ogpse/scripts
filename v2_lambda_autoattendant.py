# ---------- OpsGenie Configuration ----------
ops_genie_api_key = ""
ops_genie_api_url = "https://api.opsgenie.com"
ops_genie_responder_endpoint = "/v2/alerts/"


# Main lambda handler
def lambda_handler(event, context):
    def get_note(event):
        alert = event["alert"]
        if "The caller has entered 1" in alert["note"]:
            req_data = {
                "responder": {
                    "name": "[team_name_here]",
                    "type": "team"
                }
            }
            req = requests.post(
                ops_genie_api_url + ops_genie_endpoint + str(alert["alertId"].encode('utf-8')) + "/responders",
                json=req_data, headers={"Authorization": "GenieKey " + ops_genie_api_key})
            result = req.json()
            return "Result: " + str(result).decode('string_escape')
        if "The caller has entered 2" in alert["note"]:
            req_data = {
                "responder": {
                    "name": "[team_name_here]",
                    "type": "team"
                }
            }
            req = requests.post(
                ops_genie_api_url + ops_genie_endpoint + str(alert["alertId"].encode('utf-8')) + "/responders",
                json=req_data, headers={"Authorization": "GenieKey " + ops_genie_api_key})
            result = req.json()
            return "Result: " + str(result).decode('string_escape')
        if "The caller has entered 3" in alert["note"]:
            req_data = {
                "responder": {
                    "name": "[team_name_here]",
                    "type": "team"
                }
            }
            req = requests.post(
                ops_genie_api_url + ops_genie_endpoint + str(alert["alertId"].encode('utf-8')) + "/responders",
                json=req_data, headers={"Authorization": "GenieKey " + ops_genie_api_key})
            result = req.json()
            return "Result: " + str(result).decode('string_escape')
        if "The caller has entered 4" in alert["note"]:
            req_data = {
                "responder": {
                    "name": "[team_name_here]",
                    "type": "team"
                }
            }
            req = requests.post(
                ops_genie_api_url + ops_genie_endpoint + str(alert["alertId"].encode('utf-8')) + "/responders",
                json=req_data, headers={"Authorization": "GenieKey " + ops_genie_api_key})
            result = req.json()
            return "Result: " + str(result).decode('string_escape')
        if "The caller has entered 5" in alert["note"]:
            req_data = {
                "responder": {
                    "name": "[team_name_here]",
                    "type": "team"
                }
            }
            req = requests.post(
                ops_genie_api_url + ops_genie_endpoint + str(alert["alertId"].encode('utf-8')) + "/responders",
                json=req_data, headers={"Authorization": "GenieKey " + ops_genie_api_key})
            result = req.json()
            return "Result: " + str(result).decode('string_escape')
        if "The caller has entered 6" in alert["note"]:
            req_data = {
                "responder": {
                    "name": "[team_name_here]",
                    "type": "team"
                }
            }
            req = requests.post(
                ops_genie_api_url + ops_genie_endpoint + str(alert["alertId"].encode('utf-8')) + "/responders",
                json=req_data, headers={"Authorization": "GenieKey " + ops_genie_api_key})
            result = req.json()
            return "Result: " + str(result).decode('string_escape')
        if "The caller has entered 7" in alert["note"]:
            req_data = {
                "responder": {
                    "name": "[team_name_here]",
                    "type": "team"
                }
            }
            req = requests.post(
                ops_genie_api_url + ops_genie_endpoint + str(alert["alertId"].encode('utf-8')) + "/responders",
                json=req_data, headers={"Authorization": "GenieKey " + ops_genie_api_key})
            result = req.json()
            return "Result: " + str(result).decode('string_escape')
        if "The caller has entered 8" in alert["note"]:
            req_data = {
                "responder": {
                    "name": "[team_name_here]",
                    "type": "team"
                }
            }
            req = requests.post(
                ops_genie_api_url + ops_genie_endpoint + str(alert["alertId"].encode('utf-8')) + "/responders",
                json=req_data, headers={"Authorization": "GenieKey " + ops_genie_api_key})
            result = req.json()
            return "Result: " + str(result).decode('string_escape')
        if "The caller has entered 9" in alert["note"]:
            req_data = {
                "responder": {
                    "name": "[team_name_here]",
                    "type": "team"
                }
            }
            req = requests.post(
                ops_genie_api_url + ops_genie_endpoint + str(alert["alertId"].encode('utf-8')) + "/responders",
                json=req_data, headers={"Authorization": "GenieKey " + ops_genie_api_key})
            result = req.json()
            return "Result: " + str(result).decode('string_escape')

    print
    "Event: " + str(event).decode('string_escape')
    print
    "Context: " + str(context).decode('string_escape')

    action = event["action"]

    if str(action).lower() == "addnote":
        print
        get_note(event)