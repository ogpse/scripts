import json
import requests

headers = {"Content-Type": "application/json", "Authorization": "GenieKey "}

def lambda_handler(event, context):
    AlertName = event["AlertRuleName"]
    Query = event["SearchQuery"]
    SearchStartTime = event["SearchIntervalStartTimeUtc"]
    Count = event["ResultCount"]
    Description = event["Description"]
    Severity = event["Severity"]
    SearchResult = event["SearchResult"]
    insideTable = SearchResult["tables"]
    table_object = insideTable[0]
    rows = table_object["rows"]
    computerArray = []
    for x in rows:
        computerArray.append(x[7])

    create_OG_Alert(AlertName, Query, SearchStartTime, Count, Description, Severity, computerArray)
    insideTable = SearchResult["tables"]
    table_object = insideTable[0]
    rows = table_object["rows"]
    print(rows)
    computerArray = []
    for x in rows:
        computerArray.append(x[7])

    #create_OG_Alert(AlertName, Query, SearchStartTime, Count, Description, Severity, computerArray)


def create_OG_Alert(AlertName, Query, SearchStartTime, Count, Description, Severity, computerArray):
    URL = "https://api.opsgenie.com/v2/alerts"
    data = json.dumps(
        {
            "message": AlertName,
            "description": "Query: " + str(Query) + "\nSearchStartTime: " + str(SearchStartTime) + " \nCount: " + str(
                Count) + "\nComments: " + str(Description) + "\nSeverity: " + str(Severity) + "\nComputers: " + str(
                computerArray),
            "tags": [Severity]
        })
    req = requests.post(url=URL, headers=headers, data=data)
    response = print(req.text)
    print(response)