import json
import requests

apiKey = '212e4b53-98aa-484f-8f0e-10698baccb09'
url = 'https://api.opsgenie.com/v2/users'
headers = {'Content-Type': 'application/json', 'Authorization': 'GenieKey ' + apiKey}


def get_user_list(url):
    req = requests.get(url=url, headers=headers)
    response = req.json()
    data = response["data"]
    paging = response["paging"]
    username_list = []
    for x in data:
        username_list.append(x["username"])
    if "next" in paging:
        nextUrl = response['paging']['next']
        get_user_list(nextUrl)
    delete_users(username_list)

def delete_users(list_of_users):
    for username in list_of_users:
        if username == 'chris.seltzer@compass.com':
            print(username)
        else:
            remove_user = requests.delete(url='https://api.opsgenie.com/v2/users/' + username, headers= headers)
            print(remove_user.text)

get_user_list(url)
