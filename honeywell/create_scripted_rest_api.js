(function process( /*RESTAPIRequest*/ request, /*RESTAPIResponse*/ response) {



    var body = request.body.data;
    var inc = new GlideRecord('incident');
    var client = new OpsGenie_Client();
    var alias = body.alias;

    //added by karthick as suggested by John (Opsgenie)

    var replaceHyphens = alias.replace(/-/g, '%2D');
    var replaceColons = replaceHyphens.replace(/:/g, '%3A');
    var alertAlias = replaceColons.replace(/ /g, '%20');
    var alertFromOpsGenie = client.getAlertFromOpsGenie(alertAlias);
    var getAlertResponse = new global.JSON().decode(alertFromOpsGenie);
    var impact = getAlertResponse.data.details.impact;
    var urgency = getAlertResponse.data.details.urgency;
    var user_input = getAlertResponse.data.details.user_input;
    var caller_id = getAlertResponse.data.details.caller_id;
    var contact_type = getAlertResponse.data.details.contact_type;
    var category = getAlertResponse.data.details.category;
    var business_service = getAlertResponse.data.details.business_service;
    var assignment_group = getAlertResponse.data.details.assignment_group;
    var integration_name = getAlertResponse.data.details.integration_name; //Added by Mary
    var operational_status;
    var details_operational_status;
    var contentMapOperationalStatus;


    gs.info("OpsGenie Create Start");

    try {

        var ci_entity = client.queryEntity('cmdb_ci', 'name', user_input);

        gs.info("OpsGenie Create ci_entity operational_status" + ci_entity.operational_status);

        if (ci_entity.operational_status != undefined && ci_entity.operational_status == 1) {

            inc.short_description = body.description;
            inc.urgency = urgency;
            inc.impact = impact;
            inc.user_input = user_input;
            inc.caller_id = caller_id;
            inc.contact_type = contact_type;
            inc.category = category;
            inc.business_service = business_service;
            inc.assignment_group = assignment_group;
            inc.u_integration_name = integration_name; //Added by Mary
            inc.short_description = body.shortDescription;
            inc.description = body.description;
            inc.x_86994_opsgenie_alert_alias = body.alias;

            inc.insert();

            //added by Karthick as per Ernesto Email

            var incidentNumber = inc.getDisplayValue();
            gs.info("OpsGenie Create Incident: " + incidentNumber);
            var details = {

                IncidentNumber: incidentNumber

            };

            var contentMap = {

                details: details

            };


            operational_status = "Operational";
            details_operational_status = {

                OperationalStatus: operational_status

            };

            contentMapOperationalStatus = {

                details: details_operational_status

            };


            client.addDetailsToOpsGenie(alertAlias, contentMap);
            client.addDetailsToOpsGenie(alertAlias, contentMapOperationalStatus);

            //added by Karthick as per Ernesto Email


            var incidentLink = gs.getProperty('glide.servlet.uri') + inc.getLink(false);
            var sysId = inc.getUniqueValue();

            gs.info("OpsGenie Create Incident sysId: " + sysId);

            return {

                "incidentLink": incidentLink,
                "sysId": sysId

            };

        } else {

            operational_status = "Non-Operational";
            details_operational_status = {

                OperationalStatus: operational_status

            };
            contentMapOperationalStatus = {

                details: details_operational_status

            };
            client.addDetailsToOpsGenie(alertAlias, contentMapOperationalStatus);



        }


    } catch (ex) {
        gs.info(ex);
    }



})(request, response);