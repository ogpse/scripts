(function transformRow(source, target, map, log, isUpdate) {
    log.info("OpsGenie start...");

    var client = new OpsGenie_Client();
    var alertId = source.u_opsgenie_alert_id;
    var alert = client.getAlertFromOpsGenie(alertId);
    var getAlertResponse = new global.JSON().decode(alert);
    var groupName = source.u_assigned_group;
    var configurationItem;
    var category;
    var impact;
    var urgency;

    log.info("OpsGenie alertId: '" + alertId.toString() + "'");
    log.info("OpsGenie alert: '" + alert.toString() + "'");
    log.info("OpsGenie response: '" + getAlertResponse.toString() + "'");
    log.info("OpsGenie data: '" + getAlertResponse.data + "'");
    log.info("configurationItem : '" + getAlertResponse.data.details.ConfigurationItem.toString() + "'");

    try {
        configurationItem = getAlertResponse.data.details.ConfigurationItem.toString(); // caps
    } catch (err) {
        log.warn("Configuration item field could not be set.");
    }

    try {
        category = getAlertResponse.data.details.Category.toString();
    } catch (err) {
        log.warn("Category field could not be set.");
    }

    try {
        impact = getAlertResponse.data.details.Impact; // int
    } catch (err) {
        log.warn("Impact field could not be set.");
    }

    try {
        urgency = getAlertResponse.data.details.Urgency;
    } catch (err) {
        log.warn("Urgency field could not be set.");
    }

    // get group name sys_id
    if (groupName !== "") {
        var groupEntity = client.queryEntity('sys_user_group', 'name', groupName);

        if (groupEntity !== undefined) {
            target.assignment_group = groupEntity.sys_id.toString();
        } else {
            log.warn("[OpsGenie Web Svc Create] Unable to find group with name \"" + groupName + "\".");
        }
    }

    // get CI sys_id
    if (configurationItem !== "") {
        var ci_entity = client.queryEntity('cmdb_ci', 'name', configurationItem);

        if (ci_entity !== undefined) {
            target.cmdb_ci = ci_entity.sys_id.toString();
        } else {
            log.warn("[OpsGenie Web Svc Create] Unable to find CI with name \"" + configurationItem + "\".");
        }
    }

    // get category sys_id
    if (category !== "") {
        var categoryEntity = client.queryEntity('u_category', 'u_short_name', category);

        if (categoryEntity !== undefined) {
            target.category = categoryEntity.sys_id.toString();
        } else {
            log.warn("[OpsGenie Web Svc Create] Unable to find category with name \"" + category + "\".");
        }
    }

    // set data
    target.u_call_back_number = "(213) 740-5555";
    target.impact = impact;
    target.urgency = urgency;

    log.info("OpsGenie end.");

})(source, target, map, log, action === "update");