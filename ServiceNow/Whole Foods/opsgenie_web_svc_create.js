(function transformRow(source, target, map, log, isUpdate) {

    var client = new OpsGenie_Client();
    var groupName = source.u_assigned_group;
    var alias = source.u_opsgenie_alert_alias;
    var alert = client.getAlertFromOpsGenie(alias);
    var getAlertResponse = new global.JSON().decode(alert);
    var opsgeniePriority = getAlertResponse.data.priority;
    
    if (opsgeniePriority == "P1") {
        target.priority = 1;
        target.impact = 1;
        target.urgency = 1;
    } else if (opsgeniePriority == "P2") {
        target.priority = 2;
        target.impact = 2;
        target.urgency = 2;
    } else if (opsgeniePriority == "P3") {
        target.priority = 3;
        target.impact = 3;
        target.urgency = 3;
    } else if (opsgeniePriority == "P4") {
        target.priority = 4;
        target.impact = 4;
        target.urgency = 4;
    } else if (opsgeniePriority == "P5") {
        target.priority = 5;
        target.impact = 5;
        target.urgency = 5;
    }

    if (groupName !== "") {
        var groupEntity = client.queryEntity('sys_user_group', 'name', groupName);

        if (groupEntity !== undefined) {
            target.assignment_group = groupEntity.sys_id.toString();

        } else {
            log.warn("[OpsGenie Web Svc Create] Unable to find group with name \"" + groupName + "\".");
        }

    }

    target.location = "79d682fe4f344300718356701310c7ca";
    target.u_caller_number = "(512) 555-5555";
    target.caller_id = "0dfdf69bdb2aab40b0b9a9954b96193c";
    target.u_affected_user = "0dfdf69bdb2aab40b0b9a9954b96193c";

    //target.u_affected_user = "0dfdf69bdb2aab40b0b9a9954b96193c";

})(source, target, map, log, action === "update");