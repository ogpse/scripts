(function executeRule(current, previous /*null when async*/ ) {

    var shortDescription = current.short_description.toString();
    var priority = current.priority.getDisplayValue().toString();
    var incidentNumber = current.number.toString();
    var openedAt = current.opened_at.toString();
    var category = current.category.toString();
    var severity = current.severity.getDisplayValue().toString();
    var state = current.state.getDisplayValue().toString();
    var assignedTo = current.assigned_to.name.toString();
    var assignedToEmail = current.assigned_to.email.toString();
    var assignmentGroup = current.assignment_group.name.toString();
    var workNotes = current.work_notes.getJournalEntry(1);
    var incidentLink = current.getLink(false);
    var sysId = current.getUniqueValue();
    var alertAlias = current.x_86994_opsgenie_alert_alias.toString();
    var configurationItem = current.cmdb_ci.getDisplayValue().toString();
    var companyName = current.company.name.toString();
    var callerName = current.caller_id.name.toString();
    var sysDomain = current.sys_domain.toString();
    var location = current.location.toString();
    var client = new OpsGenie_Client();

    var contentMap = {
        "alias": incidentNumber,
        "incidentNumber": incidentNumber,
        "shortDescription": shortDescription,
        "priority": priority,
        "openedAt": openedAt,
        "category": category,
        "severity": severity,
        "state": state,
        "assignedTo": assignedTo,
        "assignedToEmail": assignedToEmail,
        "assignmentGroup": assignmentGroup,
        "workNotes": workNotes,
        "teams": client.replaceForbiddenCharacters(assignmentGroup),
        "recipients": assignedToEmail,
        "configurationItem": configurationItem,
        "companyName": companyName,
        "callerName": callerName,
        "sysDomain": sysDomain,
        "incidentLink": gs.getProperty('glide.servlet.uri') + incidentLink,
        "sysId": sysId,
        "location": location

    };

    client.postToOpsGenie(contentMap);
    current.x_86994_opsgenie_alert_alias = incidentNumber;
    current.update();

})(current, previous);