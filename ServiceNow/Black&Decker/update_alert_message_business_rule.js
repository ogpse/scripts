(function executeRule(current, previous /*null when async*/) {
	var alertId = current.u_opsgenie_alert_id.toString();
	var incidentNumber = current.number.toString();
	var message = current.short_description.toString();
	var updatedMessage = message + " - " + incidentNumber;
	var client = new OpsGenie_Client();
	
	var contentMap = {
		"message": updatedMessage,
	};
	
	client.updateOpsgenieAlertMessage(contentMap, alertId);
})(current, previous);