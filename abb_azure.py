import json
import requests

headers = {"Content-Type": "application/json", "Authorization": "GenieKey 9510b5b9-6e41-44f6-8830-f77c9b185a2e"}


def lambda_handler(event, context):
    print(event)
    AlertName = event["AlertRuleName"]
    Query = event["SearchQuery"]
    AlertThresholdOperator = event["AlertThresholdOperator"]
    AlertThresholdValue = event["AlertThresholdValue"]
    SearchIntervalInSeconds = event["SearchIntervalInSeconds"]
    SubscriptionId = event["SubscriptionId"]
    SearchStartTime = event["SearchIntervalStartTimeUtc"]
    Count = event["ResultCount"]
    Description = event["Description"]
    Severity = event["Severity"]
    SearchResult = event["SearchResult"]
    LinkToSearchResults = event["LinkToSearchResults"]
    insideTable = SearchResult["tables"]
    print(insideTable)
    table_object = insideTable[0]
    rows = table_object["rows"]
    computerArray = []
    for x in rows:
        print(x)
        computerArray.append(x)
    print(computerArray)

    create_OG_Alert(AlertName, Query, SearchStartTime, Count, Description, Severity, computerArray,
                    AlertThresholdOperator, AlertThresholdValue, SubscriptionId, SearchIntervalInSeconds,
                    LinkToSearchResults)


def create_OG_Alert(AlertName, Query, SearchStartTime, Count, Description, Severity, computerArray,
                    AlertThresholdOperator, AlertThresholdValue, SubscriptionId, SearchIntervalInSeconds,
                    LinkToSearchResults):
    URL = "https://api.opsgenie.com/v2/alerts"
    data = json.dumps(
        {
            "message": "[Azure] " + str(Severity) + " " + str(AlertName) + " " + str(
                AlertThresholdOperator) + " " + str(AlertThresholdValue) + " exceeded  - " + str(
                Count) + " events occurred in the last " + str(SearchIntervalInSeconds) + " seconds",
            "description": "Query: " + str(Query) + "\nSearchStartTime: " + str(SearchStartTime) + " \nCount: " + str(
                Count) + "\nComments: " + str(Description) + "\nSeverity: " + str(
                Severity) + "\nCol1, Col2, Col3: " + str(
                computerArray),
            "tags": [Severity],
            "details": {
                "Query": str(Query),
                "AlertRuleName": str(AlertName),

                "SubscriptionId": str(SubscriptionId),
                "SearchStartTime": str(SearchStartTime),
                "Count": str(Count),
                "Severity": str(Severity),
                "Comments": str(Description),
                "ResultsURL": "<a href=" + str(LinkToSearchResults) + ">Click Here</a>",
                "Col1, Col2, Col3": str(computerArray)
            }
        })
    req = requests.post(url=URL, headers=headers, data=data)
    response = print(req.text)

