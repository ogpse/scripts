import requests
import json
import csv
import re

OG_KEY = '212e4b53-98aa-484f-8f0e-10698baccb09'
OG_HEADERS = {'Content-Type': 'application/json', 'Authorization': 'GenieKey ' + OG_KEY}
PD_KEY = ''
PD_HEADERS = {'Accept': 'application/vnd.pagerduty+json;version=2', 'Authorization': 'Token token=' + PD_KEY}
OG_ACCOUNT = 'compass_tech1'
PD_ACCOUNT = ''
datadog_token = ''

service_list = []
list_for_csv =[]

og_integrations_type_dict = {
    'Amazon CloudWatch': 'CloudWatch',
    'Datadog': 'Datadog',
    'events_api_v2_inbound_integration': 'API',
    'generic_email_inbound_integration': 'Email',
    'Pingdom': 'PingdomV2',
    'Meraki-Alerts-Transformer': 'API',
    'API': 'API',
    'Generic API': 'API',
    'Google Form': 'API',
    'Jira': 'Jira',
    'Activity Service': 'API',
    'Activity Subscribers': 'API',
    'airtable': 'API',
    'Amazon RDS': 'AmazonRds',
    'Atlas Integration': 'API',
    'AWS CloudTrail': 'AmazonCloudTrail',
    'Circuit Breaker': 'API',
    'Cloudwatch': 'CloudWatch',
    'cloudwatch-search-infra-low-prio': 'CloudWatch',
    'Contact Email Service': 'API',
    'Contacts Subscribers': 'API',
    'datadog': 'Datadog',
    'Datadog - Contacts Service': 'Datadog',
    'Datadog - Tasks Production': 'Datadog',
    'Datadog Jenkins Monitor': 'Datadog',
    'Datadog Search Infra': 'Datadog',
    'Datadog SEO Services': 'Datadog',
    'Datadog Synthetics': 'Datadog',
    'datadog-devops': 'Datadog',
    'Datadog-MessierService-Production': 'Datadog',
    'Datadog-Search-Actions-Share-Action': 'Datadog',
    'datadog-search-infra-high-prio': 'Datadog',
    'datadog-search-infra-low-prio': 'Datadog',
    'Feed Service': 'API',
    'Firebase': 'API',
    'Growth Infra Datadog': 'Datadog',
    'Jenkins': 'Jenkins',
    'Jenkins CI': 'Jenkins',
    'Jenkins Production': 'Jenkins',
    'Jenkins Staging': 'Jenkins',
    'jenkins staging build': 'Jenkins',
    'librato-search-infra': 'Librato',
    'librato-search-infra-high-prio': 'Librato',
    'librato-search-infra-low-prio': 'Librato',
    'Logentries - Clockwork Stopped': 'Logentries',
    'Loggly': 'Loggly',
    'Mongo-Atlas-Alert': 'MongoDBCloud',
    'Mongo-Atlas-Production-Listing-Data': 'MongoDBCloud',
    'MongoDB Cloud Manager': 'MongoDBCloud',
    'New Relic': 'NewRelicV2',
    'Omni Suggest Critical': 'API',
    'Omni Suggest Non Critical': 'API',
    'Pingdom search-infra': 'PingdomV2',
    'Pipeline': 'API',
    'Search History Service - Critical': 'API',
    'Search History Service - Non Critical': 'API',
    'Search Infra AWS CloudWatch': 'CloudWatch',
    'Search Suggest V2 Critical': 'API',
    'Search Suggest V2 Non Critical': 'API',
    'Search Typeahead Component Critical': 'API',
    'Search Typeahead Component Non Critical': 'API',
    'Sentry': 'Sentry',
    'QS-Cloudwatch-Metrics': 'CloudWatch',
    'Suggestions Service': 'API',
    'uc-frontend--consumer-search : Pingdom': 'PingdomV2',
    'uc-frontend--search : Pingdom': 'PingdomV2',
    'Logstash': 'Logstash'
}
pd_integration_types = []


def all_pd_services():
    page = 0
    url = f'https://api.pagerduty.com/services?time_zone=UTC&sort_by=name&include%5B%5D=integrations&offset={page}&total=true'
    response = requests.get(url=url, headers=PD_HEADERS).json()
    total = response['total']
    while page < total:
        url = f'https://api.pagerduty.com/services?time_zone=UTC&sort_by=name&include%5B%5D=integrations&offset={page}&total=true'
        json_services = requests.get(url= url, headers= PD_HEADERS).json()
        list_of_services = json_services['services']
        page += json_services['limit']
        if page == json_services['total']:
            break
        for service in list_of_services:
            name = service['name']
            service_id = service['id']
            list_of_integrations = service['integrations']
            print((list_of_integrations))
            for integration in list_of_integrations:
                pd_integration_type = integration['type']
                pd_integration_summary = integration['summary']
                pd_integration_link = integration['self']
                if len(service['teams']) is 0:
                    team = ''
                    pd_integration_list = [name, pd_integration_summary, pd_integration_link, team]
                else:
                    team = service['teams'][0]['summary'].replace('-', ' ')
                    pd_integration_list = [name, pd_integration_summary, pd_integration_link, team]
                if pd_integration_type == 'generic_email_inbound_integration':
                    integration_id = integration['id']
                    integration_url = 'https://api.pagerduty.com/services/' + service_id + '/integrations/' + integration_id
                    req = requests.get(url=integration_url, headers=PD_HEADERS).json()
                    pd_integration_email = req['integration']['integration_email']
                    pd_integration_list = [name, pd_integration_summary, pd_integration_link, team, pd_integration_email]
                    add_og_email_integration(name, pd_integration_summary, pd_integration_type, pd_integration_email, team, pd_integration_list)
                add_og_api_integration(name, pd_integration_summary, pd_integration_type, team, pd_integration_list)

def add_og_email_integration(name_of_service,specific_type_of_integration,type,email_address,team,pd_integration_list):
    url = 'https://api.opsgenie.com/v2/integrations'
    if type in og_integrations_type_dict:
        integration_type = og_integrations_type_dict[type]
        name_of_integration = name_of_service + ' - ' + specific_type_of_integration
        data = {'type': integration_type, 'name': name_of_integration, 'emailUsername': email_address.split('@')[0]}
        if team is '':
            req = requests.post(url=url, headers=OG_HEADERS, data=json.dumps(data))
            response = req.text
            json_response = req.json()
            og_email_address = json_response['data']['emailAddress']
            name_email = [name_of_integration, og_email_address]
            pd_integration_list.extend([og_email_address])
            list_for_csv.extend(name_email)
            print(response)
        else:
            data['ownerTeam'] = {'name': team}
        req = requests.post(url= url, headers = OG_HEADERS, data = json.dumps(data))
        response = req.text
        json_response = req.json()
        teamId = json_response['data']['teamId']
        og_email_address = json_response['data']['emailAddress']
        name_email = [name_of_integration, og_email_address]
        pd_integration_list.extend([teamId, name_of_integration, og_email_address])
        list_for_csv.extend(name_email)
        print(response)


def add_og_api_integration(name_of_service, specific_type_of_integration, type, team, pd_integration_list):
    url = 'https://api.opsgenie.com/v2/integrations'
    if type == 'nagios_inbound_integration':
        integration_type = 'NagiosV2'
        name_of_integration = name_of_service + ' - ' + specific_type_of_integration
        data = {'type': integration_type, 'name': name_of_integration}
        if team is '':
            req = requests.post(url=url, headers=OG_HEADERS, data=json.dumps(data))
            response = req.text
            json_response = req.json()
            og_api_key = json_response['data']['apiKey']
            name_key = [name_of_integration, og_api_key]
            pd_integration_list.extend([name_of_integration, og_api_key])
            list_for_csv.extend(name_key)
            print(response)
        else:
            data['ownerTeam'] = {'name': team}
        req = requests.post(url=url, headers=OG_HEADERS, data=json.dumps(data))
        response = req.text
        json_response = req.json()
        teamId = json_response['data']['teamId']
        og_api_key = json_response['data']['apiKey']
        name_key = [name_of_integration, og_api_key]
        pd_integration_list.extend([teamId, name_of_integration, og_api_key])
        list_for_csv.extend(name_key)
        print(response)
    if specific_type_of_integration in og_integrations_type_dict:
        integration_type = og_integrations_type_dict[specific_type_of_integration]
        name_of_integration = name_of_service + ' - ' + specific_type_of_integration
        data = {'type': integration_type, 'name': name_of_integration}
        if team is '':
            if integration_type == 'Datadog':
                data['token'] = datadog_token
                data['updatesActionMappings'] = [
                    {
                    "action":"acknowledge",
                    "extraField":"",
                    "extraFieldForMappedAction":"",
                        "mappedAction":"acknowledge"
                    },
                    {"action":"close",
                     "extraField":"",
                     "extraFieldForMappedAction":"",
                     "mappedAction":"close"
                     }
                                                 ]
            req = requests.post(url=url, headers=OG_HEADERS, data=json.dumps(data))
            response = req.text
            json_response = req.json()
            og_api_key = json_response['data']['apiKey']
            pd_integration_list.extend([name_of_integration, og_api_key])
            list_for_csv.extend([name_of_integration, og_api_key])
            print(response)
        else:
            data['ownerTeam'] = {'name': team}
        if integration_type == 'Datadog':
            data['token'] = datadog_token
            data['updatesActionMappings'] = [
                    {
                    "action":"acknowledge",
                    "extraField":"",
                    "extraFieldForMappedAction":"",
                        "mappedAction":"acknowledge"
                    },
                    {"action":"close",
                     "extraField":"",
                     "extraFieldForMappedAction":"",
                     "mappedAction":"close"
                     }
                                                 ]
        req = requests.post(url= url, headers=OG_HEADERS, data= json.dumps(data))
        response = req.text
        json_response = req.json()
        if 'data' in json_response:
            teamId = json_response['data']['teamId']
            og_api_key = json_response['data']['apiKey']
            pd_integration_list.extend([teamId, name_of_integration, og_api_key])
            list_for_csv.extend([name_of_integration,og_api_key])
            print(response)

def main():
    all_pd_services()
    print(list_for_csv)
    #need to test the list for csv and addition of the teams

main()


