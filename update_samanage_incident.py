import json
import requests

headers = {"Content-Type": "application/json", "X-Samanage-Authorization": 'Bearer '
def lambda_handler(event, context):
   print "Event" + str(event)
   alert = event["alert"]
   name = alert["message"]
   priority = alert["priority"]
   tags = alert["tags"]
   incidentId =tags[0]
   samanage_priority = ""
   if priority == "P1":
       samanage_priority = "Critical"
   elif priority == "P2":
       samanage_priority = "High"
   elif priority == "P3":
       samanage_priority = "Medium"
   elif priority == "P4":
       samanage_priority = "Low"
   else:
       samanage_priority = "none"

    updateSamanagePriority(samanage_priority,incidentId)


def updateSamanagePriority(samanage_priority,incidentId):
    URL = "https://api.samanage.com/incidents/" + incidentId + ".json"
    data = json.dumps({
        "incident": {
            "priority": samanage_priority
        }
    })

    req = requests.put(URL, headers= headers, data= data)
    response = req.json()
    return "Response: " + str(response).decode('string_escape')
