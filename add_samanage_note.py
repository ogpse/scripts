import json
import requests

headers = {"Content-Type": "application/json", "X-Samanage-Authorization": 'Bearer '}

def lambda_handler(event, context):
   print "Event" + str(event)
   alert = event["alert"]
   name = alert["message"]
   priority = alert["priority"]
   tag = alert["tags"]
   incidentId = tag[0]
   alertNote = alert["note"]

   getIncidentDescription(incidentId, alertNote)

def getIncidentDescription(incidentId, alertNote):
    URL = "https://api.samanage.com/incidents/" + incidentId + ".json"
    req = requests.get(url=URL, headers=headers)
    response = req.json()
    print(response)
    samanageDescription = response["description"]

    addSamanageNote(alertNote,incidentId,samanageDescription)

    return "Response: " + str(response).decode('string_escape')

def addSamanageNote(alertNote,incidentId,samanageDescription):
    URL = "https://api.samanage.com/incidents/" + incidentId + ".json"
    fullDescription = samanageDescription + alertNote
    data = json.dumps({
        "incident": {
            "description": fullDescription
        }
    })

    req = requests.put(url=URL, headers= headers, data= data)
    response = req.json()
    return "Response: " + str(response).decode('string_escape')
