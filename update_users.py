
import json
import requests

apiKey = ""
headers = {"Content-Type": "application/json", "Authorization": "GenieKey " + apiKey}
url = "https://api.opsgenie.com/v2/users?limit=100&order=ASC&sort=username&query=role:user"
username_list = []

def getUsernames(x):
    r = requests.get(x, headers = headers)
    data_json = r.json()
    list_of_users = data_json["data"]
    for user in list_of_users:
        username = user["username"]
        username_list.append(username)
    if "next" in data_json["paging"]:
        newURL = data_json["paging"]["next"]
        getUsernames(newURL)

getUsernames(url)
def updateUserRole(y):
    data = json.dumps({
        "role": {
            "name": ""
    }
    })
    for x in username_list:
        url = "https://api.opsgenie.com/v2/users/" + x
        req = requests.patch(url, headers = headers, data = data)
        response = req.text

updateUserRole(username_list)