import urllib.parse
import urllib.request
import urllib.error
import urllib.response
import json

global debug
debug=True

global logit
logit=True


def getUserName():
    initialURL = "https://api.opsgenie.com/v2/users?limit=100&order=ASC&sort=username"
    headers = {"Content-Type": "application/json", "Authorization": "GenieKey "}
    req = urllib.request.Request(initialURL, headers=headers)    
    result = urllib.request.urlopen(req)
    messages = result.read()
    json_obj = json.loads(messages)

    UserData=[json_obj]

    while 'next' in json_obj['paging']:
        nextURL=str(json_obj['paging']['next'])
        req = urllib.request.Request(nextURL, headers=headers)    
        result = urllib.request.urlopen(req)
        messages = result.read()
        json_obj = json.loads(messages)
        UserData.append(json_obj)

    mobilecount = 0
    totalcount = 0

    try:
        for users in UserData:
    
            for x in users['data']:
                username = x["username"]
                fullname = x["fullName"]
                mobilecount=listMobileUsers(username, fullname, headers)
                if mobilecount == 1:
                    totalcount += 1

        print ("\n")
        print ("Total number of users using the mobile app: "+str(totalcount))
    except IOError as e:
        print (e)

def listMobileUsers(username, fullname, headers):
    url="https://api.opsgenie.com/v2/users/"+username+"/contacts"
    mobilecount=0
    try:
        req = urllib.request.Request(url, headers=headers)    
        result = urllib.request.urlopen(req)
        messages = result.read()
        json_obj = json.loads(messages)
        for x in json_obj["data"]:
            method = x["method"]
            device = x["to"]
            # print(method + ": " + device + "\n")
            if "mobile" in method:
                mobilecount+=1
                print (fullname + " is using",device)
            if mobilecount == 1:
                return(mobilecount)

            
    except IOError as f:
        print (f)

    return(mobilecount)

def main():
    getUserName()

if __name__== "__main__":
    main()
