import csv
import requests
import json

headers = {"Content-Type": "application/json", "Authorization": "GenieKey "}

def fileReader():
    filename = ''
    with open(filename) as f:
        reader = csv.DictReader(f)
        for row in reader:
            username = row["username"]
            phoneNumber = row["cellPhone"]
            addSms(username, phoneNumber)


def addSms(username, phoneNumber):
    lowerUsername = username.lower()
    url = "https://api.opsgenie.com/v2/users/" + lowerUsername + "/contacts"
    data = json.dumps({
            'method': 'sms',
            'to': phoneNumber
        })

    r = requests.post(url=url,headers=headers,data=data)
    print(r.text)

    addVoice(username,phoneNumber)

def addVoice(username, phoneNumber):
    lowerUsername = username.lower()
    url = "https://api.opsgenie.com/v2/users/" + lowerUsername + "/contacts"
    data = json.dumps({
        'method': 'voice',
        'to': phoneNumber
    })

    r = requests.post(url=url, headers=headers, data=data)
    print(r.text)

fileReader()


