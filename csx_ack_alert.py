import json
import requests

ack_URL = "http://tcis-uat.csx.com/v2/com/opsgenie.cfc?method=AcknowledgeAlert&ln="
#"TCISGroupname=TCIS-APP-SUPPORT&ln=234234&ackid=aaa111222&userid=scott_ritter@csx.com&ackid=”

def lambda_handler(event, context):
    alert_json = event['alert']
    alertId = alert_json['alertId']
    tinyid = alert_json['tinyId']
    username = alert_json['username']
    tag = alert_json['tags'][0]
    req = requests.post(url = ack_URL + str(tinyid))
    print(ack_URL + str(tinyid))
    response = req.text
    print(response)