import csv
import requests
import json
import pandas as pd
import sys

## Enter API key to access OpsGenie account below ##
api_Key = 'Paste API key from OpsGenie here'
listOfUsers = 'Enter CSV file name for list of users here'

def CustomContacts(userList_csv,ApiKey):

    Users = pd.read_csv(userList_csv)
    baseURL = "https://api.opsgenie.com/v2/users/"
    API_Headers = {'Content-Type': 'application/json','Authorization':'GenieKey ' + ApiKey}

    for i in range(0,len(Users)):
        userLogin = Users.iloc[i]['username']
        userRole = Users.iloc[i]['roleName']
        
        userURL = baseURL+userLogin
        customRole = {'details':{'role':[userRole]} }

        customBody = json.dumps(customRole)

        addCustom = requests.patch(url = userURL, data = customBody, headers = API_Headers)
    return addCustom

CustomContacts(listOfUsers, api_Key)

