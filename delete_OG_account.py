import requests

og_api_key = 'dec21415-5aba-45aa-848a-7bc1f6a4fdd1'
headers = {'Content-Type': 'application/json', 'Authorization': 'GenieKey ' + og_api_key}
owner_username = 'colin.j.saunders60@gmail.com'
#need to add pagination for each
def delete_teams():
    team_list_raw = requests.get('https://api.opsgenie.com/v2/teams', headers=headers)
    teams_list = team_list_raw.json()['data']
    for x in teams_list:
        remove_team_req = requests.delete('https://api.opsgenie.com/v2/teams/' + x['id'], headers=headers)
        print(remove_team_req.text)

def delete_escalations():
    unowned_escalations = requests.get('https://api.opsgenie.com/v2/escalations', headers=headers)
    esclation_list = unowned_escalations.json()['data']
    for x in esclation_list:
        remove_escalation = requests.delete('https://api.opsgenie.com/v2/escalations/' + x['id'], headers=headers)
        print(remove_escalation.text)

def delete_schedules():
    unowned_schedules = requests.get('https://api.opsgenie.com/v2/schedules', headers=headers)
    schedule_list = unowned_schedules.json()['data']
    for x in schedule_list:
        remove_schedule = requests.delete('https://api.opsgenie.com/v2/schedules/' + x['id'], headers=headers)
        print(remove_schedule.text)

def delete_policies():
    global_policies = requests.get('https://api.opsgenie.com/v2/policies/alert', headers=headers)
    policy_list = global_policies.json()['data']
    for x in policy_list:
        remove_policy = requests.delete('https://api.opsgenie.com/v2/policies/' + x['id'], headers=headers)
        print(remove_policy.text)

def delete_maintenance():
    non_expired_maintenance = requests.get('https://api.opsgenie.com/v1/maintenance?type=non-expired', headers=headers)
    list_of_maintenance = non_expired_maintenance.json()['data']
    for x in list_of_maintenance:
        remove_maintenance = requests.delete('https://api.opsgenie.com/v1/maintenance/' + x['id'], headers=headers)
        print(remove_maintenance.text)

def delete_roles():
    user_roles = requests.get('https://api.opsgenie.com/v2/roles', headers=headers)
    list_of_roles =user_roles.json()['data']
    for x in list_of_roles:
        remove_roles = requests.delete('https://api.opsgenie.com/v2/roles/:identifier' + x['id'], headers=headers)
        print(remove_roles)
#delete_teams()

#delete_escalations()

#delete_schedules()

#delete_policies()

#delete_maintenance()