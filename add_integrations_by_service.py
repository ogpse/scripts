import requests
import json

OG_KEY = ''
OG_HEADERS = {'Content-Type': 'application/json', 'Authorization': 'GenieKey ' + OG_KEY}
PD_KEY = ''
PD_HEADERS = {'Accept': 'application/vnd.pagerduty+json;version=2', 'Authorization': 'Token token=' + PD_KEY}
OG_ACCOUNT = ''
PD_ACCOUNT = ''


service_list = []


og_integrations_type_dict = {
    'Amazon CloudWatch': 'CloudWatch',
    'Datadog': 'Datadog',
    'Sentry': 'Sentry',
    'Jenkins': 'Jenkins',
    'JIRA': 'Jira',
    'Rigor': 'Rigor',
    'JIRA Software': 'Jira',
    'Zendesk': 'Zendesk',
    'events_api_v2_inbound_integration': 'API',
    'generic_email_inbound_integration': 'Email',
    'Pingdom': 'PingdomV2',
    'API': "API",
    'Generic API': 'API',
    'Logstash': 'Logstash'
}
pd_integration_types =[]


def all_pd_services():
    page = 0
    url = f'https://api.pagerduty.com/services?time_zone=UTC&sort_by=name&include%5B%5D=integrations&offset={page}&total=true'
    response = requests.get(url=url, headers=PD_HEADERS).json()
    total = response['total']
    while page < total:
        url = f'https://api.pagerduty.com/services?time_zone=UTC&sort_by=name&include%5B%5D=integrations&offset={page}&total=true'
        json_services = requests.get(url= url, headers= PD_HEADERS).json()
        list_of_services = json_services['services']
        service_list.extend(list_of_services)
        page += json_services['limit']
        if page == json_services['total']:
            break
        for service in service_list:
            name = service['name']
            service_id = service['id']
            list_of_integrations = service['integrations']
            print(list_of_integrations)
            for integration in list_of_integrations:
                pd_integration_type = integration['type']
                pd_integration_summary = integration['summary']
                if len(service['teams']) is 0:
                    team = ''
                else:
                    team = service['teams'][0]['summary'].replace('-',' ').replace('(',' ').replace(')',' ')
                if pd_integration_type == 'generic_email_inbound_integration':
                    integration_id = integration['id']
                    integration_url = 'https://api.pagerduty.com/services/' + service_id + '/integrations/' + integration_id
                    req = requests.get(url= integration_url, headers = PD_HEADERS).json()
                    pd_integration_email = req['integration']['integration_email']
                    add_og_email_integration(name, pd_integration_summary, pd_integration_type,pd_integration_email, team)
                add_og_api_integration(name,pd_integration_summary,pd_integration_type,team)

def add_og_email_integration(name_of_service,specific_type_of_integration,type,email_address,team):
    url = 'https://api.opsgenie.com/v2/integrations'
    if type in og_integrations_type_dict:
        integration_type = og_integrations_type_dict[type]
        name_of_integration = name_of_service + ' - ' + specific_type_of_integration
        data = { 'type': integration_type, 'name': name_of_integration, 'emailUsername': email_address.split('@')[0]}
        if team is '':
            req = requests.post(url=url, headers=OG_HEADERS, data=json.dumps(data))
            response = req.text
            json_response = req.json()
            if 'data' in json_response:
                og_email_address = json_response['data']['emailAddress']
                list_to_print = [name_of_service, name_of_integration, email_address, og_email_address]
                print(list_to_print)
                print(response)
        else:
            data['ownerTeam'] = {'name': team}
        req = requests.post(url= url, headers = OG_HEADERS, data = json.dumps(data))
        response = req.text
        json_response = req.json()
        if 'data' in json_response:
            teamId = json_response['data']['teamId']
            og_email_address = json_response['data']['emailAddress']
            list_to_print = [name_of_service, name_of_integration, email_address, og_email_address, teamId, team]
            print(list_to_print)
            print(response)


def add_og_api_integration(name_of_service,specific_type_of_integration,type,team):
    url = 'https://api.opsgenie.com/v2/integrations'
    if specific_type_of_integration in og_integrations_type_dict:
        integration_type = og_integrations_type_dict[specific_type_of_integration]
        name_of_integration = name_of_service + ' - ' + specific_type_of_integration
        data = { 'type': integration_type, 'name': name_of_integration}
        if team is '':
            if integration_type == 'Datadog':
                data['token'] = ''
            req = requests.post(url=url, headers=OG_HEADERS, data=json.dumps(data))
            response = req.text
            print(response)
            json_response = req.json()
            if 'data' in json_response:
                og_api_key = json_response['data']['apiKey']
                list_to_print = [name_of_service, name_of_integration, og_api_key]
                print(list_to_print)
                print(response)
        else:
            data['ownerTeam'] = {'name': team}
        if integration_type == 'Datadog':
            data['token'] = ''
        req = requests.post(url= url, headers=OG_HEADERS, data= json.dumps(data))
        response = req.text
        json_response = req.json()
        if 'data' in json_response:
            teamId = json_response['data']['teamId']
            og_api_key = json_response['data']['apiKey']
            list_to_print = [name_of_service, name_of_integration, og_api_key, teamId, team]
            print(list_to_print)
            print(response)
        # print(json_response)
        # teamId = json_response['data']['teamId']
        # og_api_key = json_response['data']['apiKey']
        # list_to_print = [name_of_service, name_of_integration, og_api_key, teamId, team]
        # print(list_to_print)
        # print(response)

all_pd_services()
