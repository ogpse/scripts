import csv
import requests
import json
import pandas as pd
import sys

# Contacts = pd.read_csv('PythonTest.csv')
# df = pd.DataFrame(data=Contacts)



# Enter API Key here #
# GenieKey = "6f034d99-317b-4a64-bfb1-638032b74e7e"


# API_Headers = {'Content-Type': 'application/json','Authorization':'GenieKey ' + ApiKey}
StartURL = "https://api.opsgenie.com/v2/users/"
EndURL = "/contacts"


def AddContacts(Spreadsheet,ApiKey):
    Contacts = pd.read_csv(Spreadsheet)
    df = pd.DataFrame(data=Contacts)
    API_Headers = {'Content-Type': 'application/json','Authorization':'GenieKey ' + ApiKey}

    for i in range(0,len(df)):
        UserLogin = (df.iloc[i][0])
        UserEmail = df.iloc[i][1]
        UserSMS = df.iloc[i][2]
        UserVoice = df.iloc[i][3]
        
        ContactURL = StartURL+UserLogin+EndURL
        EmailInfo = {'method': 'email', 'to': UserEmail}
        SMSInfo = {'method': 'sms', 'to': UserSMS}
        VoiceInfo = {'method': 'voice', 'to': UserVoice}

        EmailBody = json.dumps(EmailInfo)
        SMSBody = json.dumps(SMSInfo)
        VoiceBody = json.dumps(VoiceInfo)

        AddEmail = requests.post(url = ContactURL, data = EmailBody, headers = API_Headers)
        AddSMS = requests.post(url = ContactURL, data = SMSBody, headers = API_Headers)
        AddVoice = requests.post(url = ContactURL, data = VoiceBody, headers = API_Headers)
    

AddContacts(sys.argv[1], sys.argv[2])
