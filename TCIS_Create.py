import requests
import json
import urllib.parse

url = 'https://tcis-uat.csx.com/v2/com/opsgenie.cfc?method=createTCISEvent&msg='

alertId = '3533d65f-b555-47cf-a1be-25292de1dd8f-1575485466378'
body = {
"TCISGroup": "TCIS-APP-SUPPORT",
"AlertID": "696CB6DF-AA56-E7AC-ABB5805BA0D2FAF9",
"userid":"scott_ritter@csx.com",
"logicalname": "Logicalname_test",
"component": "component_123",
"description":"New description test",
"priority": "3",
"transactionid": "transactionid_123",
"abendcode":"ERR123",
"system": "TCIS"
}

def post_TCIS():
    req = requests.post(url=url + urllib.parse.quote(json.dumps(body)))
    response = req.text
    print(response)

post_TCIS()