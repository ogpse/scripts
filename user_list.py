import requests
import csv


OG_KEY = '1b0e2036-f62c-4663-8d47-352c6bba9171'
headers = {'Content-Type': 'application/json', 'Authorization': 'GenieKey ' + OG_KEY}
url= 'https://api.opsgenie.com/v2/users'
verification_url = 'https://api.opsgenie.com/v2/users?query=verified'

def get_user_list(url):
    req = requests.get(url=url, headers=headers)
    response = req.json()
    data = response["data"]
    paging = response["paging"]
    for x in data:
        username = x["username"]
        id = x["id"]
        print(username)
        get_user_verification(username)
        #get_user_escalations(username)
    if "next" in paging:
        nextUrl = response['paging']['next']
        get_user_list(nextUrl)

def get_user_verification(un):
    user_url = 'https://api.opsgenie.com/v2/users/' + un + '?expand=contact'
    get_user = requests.get(user_url, headers=headers)
    user_resp = get_user.json()
    print(user_resp['data']['userContacts'])


# def get_user_escalations(un):
#     esclations_url = 'https://api.opsgenie.com/v2/users/' + un +'/escalations'
#     esc_get = requests.get(esclations_url, headers=headers)
#     response = esc_get
#     print(response)
#     for x in response:
#         #esc_name = x['name']
#         #print(esc_name)
#     schedule_url = 'https://api.opsgenie.com/v2/users/' + un + '/schedules'
#     sched_get = requests.get(schedule_url, headers=headers)
#     sched_resp = sched_get['data'].json()
#     for y in sched_resp:
#         sched_name = y['name']
#         print(sched_name)
#     teams_url = 'https://api.opsgenie.com/v2/users/' + un + '/teams'
#     teams_get = requests.get(teams_url, headers=headers)
#     teams_resp = teams_get['data'].json()
#     for z in teams_resp:
#         team_name = z['name']
#         print(team_name)
#
get_user_list(url)

