(function transformRow(source, target, map, log, isUpdate) {
    var client = new OpsGenie_Client();
    var groupName = source.u_assigned_group;
    var alias = source.u_opsgenie_alert_alias;
    var alert = client.getAlertFromOpsGenie(alias);
    var getAlertResponse = new global.JSON().decode(alert);

    // Add additional OpsGenie extra property fields below
    var caller;
    var company;
    var assignedTo;
    var contactType;
    /*
        Attempt to set the caller variable from the caller extra
        property in OpsGenie.
    */
    try {
        caller = getAlertResponse.data.details.caller.toString();
    } catch (err) {
        log.warn("Caller field is empty in OpsGenie.");
    }
    /*
        Attempt to set the assignedTo variable from the "assignedTo"
        extra property in OpsGenie.
    */
    try {
        assignedTo = getAlertResponse.data.details.assignedTo.toString();
    } catch (err) {
        log.warn("assignedTo field is empty in OpsGenie.");
    }
    /*
        Attempt to set the contact type variable from the "contactType"
        extra property in OpsGenie.
    */
    try {
        contactType = getAlertResponse.data.details.contactType.toString();
        target.contact_type = contactType.toString();
    } catch (err) {
        log.warn("contactType field is empty in OpsGenie.");
    }
    /*
        If the OpsGenie team name is not null, compare it with the
        group names in ServiceNow, and if there is a match, set
        the assignedGroup.
    */
    if (groupName !== "") {
        var groupEntity;
        try {
            groupEntity = client.queryEntity('sys_user_group', 'name', groupName);
        } catch (err) {}

       if (groupEntity !== undefined) {
            target.assignment_group = groupEntity.sys_id.toString();
        } else {
            log.warn("[OpsGenie Web Svc Create] Unable to find group with name \"" + groupName + "\".");
        }
    }
    /*
        If the caller variable is not null, then grab the caller's record.
        From there, set the caller_id, sys_domain, and company fields.
    */
    if (caller !== "") {
        var callerEntity;
        var domainEntity;
        try {
            callerEntity = client.queryEntity('sys_user', 'name', caller);
        } catch (err) {}

       if (callerEntity !== undefined) {
            target.caller_id = callerEntity.sys_id.toString();
            target.sys_domain = callerEntity.sys_domain.toString();
            target.company = callerEntity.company.toString();

       } else {
            log.warn("[OpsGenie Web Svc Create] Unable to find caller with name \"" + caller + "\".");
        }
    }
    /*
        If the assignedTo variable is not null, set it's value to
        the assignedTo record in the incident table.
    */
    if (assignedTo !== "") {
        var callerName;
        try {
            callerName = client.queryEntity('sys_user', 'name', assignedTo);
        } catch (err) {}

       if (callerName !== undefined) {
            target.assigned_to = callerName.sys_id.toString();
        } else {
            log.warn("Unable to find assigndTo with name \"" + assignedTo + "\".");
        }
    }


})(source, target, map, log, action==="update");
