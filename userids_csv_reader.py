import csv
import requests
import json


apiKey = "14d3fc9e-3635-4141-948c-abeec5371bbc"
headers = {"Content-Type": "application/json", "Authorization": "GenieKey " + apiKey}

def filereader():
    filename = "UserIds.csv"
    with open(filename) as file:
        reader = csv.DictReader(file)
        number_of_users = reader.line_num-1
        for row in reader:
            user_id = row["UserIds"]


            getContactMethods(user_id, number_of_users)

    file.close()

def getContactMethods(user_id, number_of_users):
    url = "https://api.opsgenie.com/v2/users/" + user_id + "/contacts"
    r = requests.get(url, headers= headers)
    contact_json = json.loads(r.text)
    print(contact_json)

    for users in contact_json:

         for contact_method in users["data"][0]:
            method = contact_method["method"]
            contact_specifics = contact_method["to"]



            createNotificationRule(user_id, method, contact_specifics)


def createNotificationRule(user_id, method, contact_specifics):
    url = "https://api.opsgenie.com/v2/users/" + user_id + "/notification-rules"

    data = json.dumps({
    "name": "Testing Notification Rule Script",
    "actionType": "create-alert",
    "criteria": {
        "type": "match-all-conditions",
        "conditions": [
            {
                "field": "message",
                "not": False,
                "operation": "contains",
                "expectedValue": "expected1"
            }
        ]
    },
    "order": 4,
    "repeat": {
        "loopAfter": 0
    },
    "steps": [
        {
            "contact": {
                "method": method,
                "to": contact_specifics
            },
            "sendAfter": {
                "timeAmount": 0
            },
            "enabled": False
        }
    ]
    })

    try:
        r = requests.post(url, headers=headers, data=data)
        response = print(r.text)
        print(response)

    except IOError as e:
        print(e)

def main():
    filereader()

main()


filereader()