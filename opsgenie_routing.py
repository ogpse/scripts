import requests
from collections import defaultdict

OG_KEY = ''
OG_HEADERS = {'Authorization': 'GenieKey ' + OG_KEY}
PD_KEY = ''
PD_HEADERS = {'Accept': 'application/vnd.pagerduty+json;version=2', 'Authorization': 'Token token=' + PD_KEY}
TEST = True
MAX_TO_RETURN = 500

#Assumptions:
#PagerDuty team names == OpsGenie team names
#PagerDuty escalation names with ' ' and '-' replaced with '_' == OpsGenie escalation names
#OpsGenie integration names are formatted like '{team} {service["summary"]} {integration["summary"]} migrated from PagerDuty'
#Set TEST to False to make real changes

def opsgenie_get_all(api_request):
    items = []
    while True:
        result = requests.get(api_request, headers=OG_HEADERS).json()
        items.extend(result['data'])
        try:
            api_request = result['paging']['next']
        except:
            break #no next page
    return items

def pagerduty_get_all(api_request, list_key):
    items = []
    page = 0
    while True:
        url = api_request + f'&offset={page}&total=true'
        result = requests.get(url, headers=PD_HEADERS).json()
        items.extend(result[list_key])
        page += result['limit']
        if page >= result['total'] or page >= MAX_TO_RETURN:
            break
    return items

og_escalations = opsgenie_get_all('https://api.opsgenie.com/v2/escalations')
escalation_map = {}
for escalation in og_escalations:
    escalation_map[escalation['name']] = escalation

def update_routing_rules(integration, team, escalation):
    print(f'{team} {integration} {escalation}')
    rules = opsgenie_get_all(f'https://api.opsgenie.com/v2/teams/{team}/routing-rules?teamIdentifierType=name')
    rule_name = integration + ' Routing Rule'
    for rule in rules:
        if rule['name'] == rule_name:
            print(f'Routing rule already exists for {integration} -> {escalation}')
            return #already created
    data = {'name': rule_name,
            'criteria':
                {'type': 'match-all-conditions', 'conditions':
                    [{'field': 'tags', 'operation': 'contains', 'expectedValue': integration}]
                },
            'notify': {'type': 'escalation', 'name': escalation}
            }
    if TEST:
        print(f'Need routing rule for {integration} -> {escalation}')
    else:
        response = requests.post(f'https://api.opsgenie.com/v2/teams/{team}/routing-rules?teamIdentifierType=name', json=data, headers=OG_HEADERS).json()
        print(f'Created routing rule for {integration} -> {escalation}')

og_integrations = opsgenie_get_all('https://api.opsgenie.com/v2/integrations')
integration_map = {}
for integration in og_integrations:
    integration_map[integration['name']] = integration

services = pagerduty_get_all(api_request='https://api.pagerduty.com/services?time_zone=UTC&sort_by=name&include%5B%5D=integrations',
                             list_key='services')
for service in services:
    if not service['teams']:
        continue #orphaned service
    escalation = service['escalation_policy']['summary']
    og_escalation = escalation.replace(' ','_')
    og_escalation = og_escalation.replace('-','_')
    if og_escalation not in escalation_map:
        print(f'Escalation not found in og: {og_escalation}')
        continue
    team = service['teams'][0]['summary']
    integrations = service['integrations']
    for integration in integrations:
        og_integration_name = f'{team} {service["summary"]} {integration["summary"]} migrated from PagerDuty'
        if og_integration_name not in integration_map:
            print(f'Integration not found in og: {og_integration_name}')
            continue
        integration_actions = requests.get(f'https://api.opsgenie.com/v2/integrations/{integration_map[og_integration_name]["id"]}/actions', headers=OG_HEADERS).json()['data']
        if og_integration_name not in integration_actions['create'][0]['tags']:
            integration_actions['create'][0]['tags'] += [og_integration_name]
            if TEST:
                print(f'Need tag for {og_integration_name}')
            else:
                response = requests.put(f'https://api.opsgenie.com/v2/integrations/{integration_map[og_integration_name]["id"]}/actions', json=integration_actions, headers=OG_HEADERS).json()
                print(f'Created tag for {og_integration_name}')
        else:
            print(f'Tag already exists for {og_integration_name}')
        update_routing_rules(og_integration_name, team, og_escalation)

