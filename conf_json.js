{
    "appName": "zabbix_edge_connector",
    "apiKey" : "ZABBIX_INTEGRATION_API_KEY",
    "baseUrl" : "https://api.opsgenie.com",
    "logLevel" : "debug",
    "actionMappings" : {
        "Acknowledge" : {
            "sourceType" : "local",
            "filepath" : "/var/opsgenie/scripts/zabbixActionExecutor4Revised.py",
            "flags": {
                "user":"ZABBIX_USER",
                "password":"ZABBIX_PASSWORD",
                "command_url":"http://127.0.0.1:8008/zabbix/api_jsonrpc.php",
                "logLevel":"DEBUG",
                "apiKey":"ZABBIX_INTEGRATION_API_KEY",
                "opsgenieUrl":"https://api.opsgenie.com"
            },
            "stdout" : "/var/log/opsgenie/ack.log",
            "stderr" : "/var/log/opsgenie/ack_err.log"
        }
    },
    "pollerConf" : {
        "pollingWaitIntervalInMillis" : 100,
        "visibilityTimeoutInSec" : 30,
        "maxNumberOfMessages" : 10
    },
    "poolConf" : {
        "maxNumberOfWorker": 12,
        "minNumberOfWorker": 4,
        "queueSize" : 0,
        "keepAliveTimeInMillis" : 6000,
                "monitoringPeriodInMillis" : 15000
    }
}