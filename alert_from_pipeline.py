import requests
import json

og_key="14d3fc9e-3635-4141-948c-abeec5371bbc"
og_url="https://api.opsgenie.com/v2/alerts"
headers={"Content-Type: application/json", "Authorization: GenieKey " + og_key}

def create_alert():
    data = {
    "message": "Pipeline has run successfully",
    "description": "Your bitbucket pipeline ran successfully",
    "priority": "P4",
    "alias": "BB-pipeline-1"
    }
    req = requests.port(og_url, headers=headers, data= json.dumps(data))
    response = print(req.text)

def main():
    create_alert()

main()