import json
import requests

URL = 'https://api.opsgenie.com/v2/teams'
apiKey = ''
headers = {'Content-Type': 'application/json', 'Authorization': 'GenieKey ' + apiKey}

def list_OG_teams():
    req = requests.get(url=URL,headers=headers)
    response = req.json()
    returned_data = response['data']
    listOfIds = []
    for x in returned_data:
        ID = x['id']
        listOfIds.append(ID)
    get_OG_team_members(listOfIds)

def get_OG_team_members(ID_list):
    for x in ID_list:
        get_Team_URL = URL + '/' + x
        get_Team_Req = requests.get(url=get_Team_URL, headers=headers)
        get_Team_Resp = get_Team_Req.json()

        try:
            get_Team_members = get_Team_Resp['data']['members']
        except KeyError as e:
            print("key [members] not found")

        for member_objects in get_Team_members:
            member_objects['role'] = 'admin'
        members = get_Team_members

        update_team_roles(get_Team_URL,members)

def update_team_roles(url,members):
    data = {
        'members': members
    }
    json_data = json.dumps(data)
    update_team_req = requests.patch(url=url,headers=headers,data=json_data)
    update_team_response = update_team_req.text
    print(update_team_response)

list_OG_teams()








