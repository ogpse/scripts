import json
import requests

headers = {"Content-Type": "application/json", "X-Samanage-Authorization": 'Bearer '}
opsgenie_headers = {"Content-Type": "application/json", "Authorization": "GenieKey "}
def lambda_handler(event, context):
   print "Event" + str(event)
   alert = event["alert"]
   name = alert["message"]
   priority = alert["priority"]
   alertId = alert["alertId"]
   samanage_priority = ""
   if priority == "P1":
       samanage_priority = "Critical"
   elif priority == "P2":
       samanage_priority = "High"
   elif priority == "P3":
       samanage_priority = "Medium"
   elif priority == "P4":
       samanage_priority = "Low"
   else:
       samanage_priority = "none"
   requester = {"email":"opsgenie@cabotcmp.com"}
   description = alert["description"]


   createSamanageIncident(name, samanage_priority, requester, description, alertId)

def createSamanageIncident(name, samanage_priority, requester, description,alertId):
   URL = "https://api.samanage.com/incidents.json"
   data = json.dumps({
       "incident": {
           "name": name,
           "priority": samanage_priority,
           "requester": requester,
           "description": description
       }
   })

   req = requests.post(URL, headers= headers, data= data)
   response = req.json()
   incidentId = response["id"]
   addNumberAsTag(incidentId,alertId)
   return "Response: " + str(response).decode('string_escape')

def addNumberAsTag(incidentId,alertId):
    url = "https://api.opsgenie.com/v2/alerts/" + alertId + "/tags"
    req_data = {
        "tags": [incidentId]
    }
    req = requests.post(url=url,json=req_data,headers=opsgenie_headers)
    result = req.json()
    return "Result: " + str(result).decode('string_escape')


