import json
import requests

apiKey = ''
url = 'https://api.opsgenie.com/v2/users'
headers = {'Content-Type': 'application/json', 'Authorization': 'GenieKey ' + apiKey}


def get_user_list(url):
    req = requests.get(url=url, headers=headers)
    response = req.json()
    data = response["data"]
    paging = response["paging"]
    for x in data:
        name = x["fullName"]
        id = x["id"]
        nameAndId = [name, id]
        removeUnderscore(nameAndId)
    if "next" in paging:
        nextUrl = response['paging']['next']
        get_user_list(nextUrl)

def removeUnderscore(nameAndId):
    fullName = nameAndId[0]
    id = nameAndId[1]
    if '_' in fullName:
        newFullName = fullName.split('_')[0] + ' ' + fullName.split('_')[1]
        update_user_name(newFullName, id)


def update_user_name(name,id):
    userId = id
    fullUrl = url + '/' + userId
    data = json.dumps({
        'fullName': name
    })
    req = requests.patch(url=fullUrl, headers=headers, data=data)
    response = req.text
    print(response)

def main():
    get_user_list(url)

main()


