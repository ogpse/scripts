import json
import requests

apiKey = "" #add key here

headers = {"Content-Type": "application/json", "Authorization": "GenieKey " + apiKey}

def lambda_handler(event, context):
    alert = event["alert"]
    alertId = alert["alertId"]
    description = alert["description"]
    url = "https://api.opsgenie.com/v2/alerts/" + alertId + "/notes"
    data = json.dumps({
        "user": "OG Script",
        "source": "AWS lambda",
        "note": description
    })
    req = requests.post(url=url, headers=headers, data=data)
    response = print(req.text)