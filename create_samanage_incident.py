import json
import requests

headers = {"Content-Type": "application/json", "X-Samanage-Authorization": 'Bearer '}

def readOgPayload(event, context):
    parsed_event = json.loads(event["body"])
    for x in parsed_event:
        print(x)
    alert = parsed_event["alert"]
    name = alert["message"]
    priority = alert["priority"]
    if priority == "P1":
        priority = "critical"
    elif priority == "P2":
        priority = "high"
    elif priority == "P3":
        priority = "medium"
    elif priority == "P4":
        priority = "low"
    else:
        priority = "none"
    requester = {"email":"opsgenie@cabotcmp.com"}
    description = alert["description"]


    createSamanageIncident(name, priority, requester, description)

def createSamanageIncident(name, priority, requester, description):
    URL = "https://api.samanage.com/incidents.json"
    data = json.dumps({
        "incident": {
            "name": name,
            "priority": priority,
            "requester": requester,
            "description": description
        }
    })

    req = requests.post(URL, headers= headers, data= data)
    response = print(req.text)