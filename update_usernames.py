import json
import requests

apiKey = ''
url = 'https://api.opsgenie.com/v2/users'
headers = {'Content-Type': 'application/json', 'Authorization': 'GenieKey ' + apiKey}
New_Domain = ''

def get_user_list(url):
    req = requests.get(url=url, headers=headers)
    response = req.json()
    data = response["data"]
    paging = response["paging"]
    for x in data:
        username = x["username"]
        username_list = [username]
        addNewDomain(username)
    if "next" in paging:
        nextUrl = response['paging']['next']
        get_user_list(nextUrl)

def addNewDomain(nameAndId):
    email = nameAndId[0]
    id = nameAndId[1]
    if '@' in email:
        newEmail = email.split('@')[0] + New_Domain
        update_username(newEmail, id)

def update_username(name,id):
    userId = id
    fullUrl = url + '/' + userId
    data = json.dumps({
        'username': name
    })
    req = requests.patch(url=fullUrl, headers=headers, data=data)
    response = req.text
    print(response)

def main():
    get_user_list(url)

main()